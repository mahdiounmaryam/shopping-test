import React, { useState } from 'react';
import Carousel from 'react-elastic-carousel';
import './CarouselSlider.css';
import ItemDetails from '../ItemDetails/ItemDetails';
import Blender from '../../assets/images/blender.png'
import SingleProduct from '../singleProduct/SingleProduct';
import RightArrow from '../RightArrow/RightArrow';
import LeftArrow from '../LeftArrow/LeftArrow';

const CarouselSlider = ({data}) => {
    return (
        <div className='bg-[#F7F7F7] rounded-3xl relative h-full  w-[282px]'>
            <img className='m-auto' src={Blender}/>
            <div className='absolute h-[112px] w-full bottom-0'>
                <h2 className='font-black text-base mr-[16px]'>همزن برقی Stanford</h2>
                <div className='absolute left-4 bottom-4'>
                    <span className='text-xl font-black'>4343000</span>
                    <span className='text-xs font-normal mr-[2px]'>تومان</span>
                </div>
            </div>
            <div className='percent bottom-3 right-[11px]  text-white rounded-[80px] absolute py-1 px-3'>
                <span className='text-[12px] font-bold'>34%</span>
            </div>
            <div className='absolute top-[37%]'>
                <RightArrow/>
            </div>
            <div className='absolute left-0 top-[37%]'>
                <LeftArrow/>
            </div>
        </div>
    )
}

export default CarouselSlider