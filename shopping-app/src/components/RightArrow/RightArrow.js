import React from 'react';
import bg from '../../assets/icons/rightArrowBg.svg';
import arrow from '../../assets/icons/rightArrow.svg';

const RightArrow = () => {
    return (
        <div className='relative'>
            <img src={bg}/>
            <img src={arrow} className='absolute top-[43%] left-[33%]'/>
        </div>
    )
}

export default RightArrow