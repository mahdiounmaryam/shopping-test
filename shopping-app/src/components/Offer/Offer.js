import React from 'react';
import Toothpaste from '../../assets/images/Toothpaste.png';
import Gift from '../../assets/images/gift.png';
import Pixel from '../../assets/images/pixelLarge.png';

const Offer = () => {
    return (
        <div className='flex gap-4 mt-9'>
            <div className='w-1/2'>
                <img className='rounded-3xl' src={Pixel}/>
            </div>
            <div className='flex gap-4 flex-col w-1/2'>
                <img className='rounded-3xl' src={Gift}/>
                <img className='rounded-3xl' src={Toothpaste}/>
            </div>
        </div>
    )
}

export default Offer