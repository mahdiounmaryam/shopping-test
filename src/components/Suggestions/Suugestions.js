import React from 'react';
import SectionTitle from '../SectionTitle/SectionTitle';
import RedFire from '../../assets/icons/redFire.svg';
import Nike from '../../assets/images/nike.png';
import SingleSuggestion from './singleSuggestion';
import Blender from '../../assets/images/blender.png';
import './suggestions.css';
import SingleProduct from '../singleProduct/SingleProduct';

const Suugestions = () => {
    const data=
        {
            title : 'کتونی نایک شماره 121',
            totalPrice : '4343000',
            price : '4343000',
            percent : '34',
            image : Nike
        }
    
    const times = [1,2,3,4,5,6,7,8,9]

    return (
        <div className='mt-4 h-fit'>
            <SectionTitle image={RedFire} hasShowMore={true} title='پیشنهادات شگفت انگیز'/>
            <div className='flex gap-5 mt-4 min1400:h-[368px]'>
                <div className='max1400:hidden'>
                    <SingleProduct image={Blender} title='همزن برقی Sanford' price='443000'/>
                </div>
                
                
                <div className='max1400:hidden suggestions-wrapper min1400:flex-wrap max1400:overflow-auto flex gap-6'>
                    {
                        times.map(t=>{
                            return (
                                <SingleSuggestion 
                                    key={t}
                                    title = {data.title}
                                    totalPrice={data.totalPrice}
                                    price ={data.price}
                                    percent = {data.percent}
                                    image= {data.image}
                                />
                            )
                        })
                    }
                </div>

                <div className='min1400:hidden suggestions-wrapper max1400:overflow-auto flex gap-6'>
                    {
                        times.map(t=>{
                            return (
                                <SingleSuggestion 
                                    key={t}
                                    title = {data.title}
                                    totalPrice={data.totalPrice}
                                    price ={data.price}
                                    percent = {data.percent}
                                    image= {data.image}
                                />
                            )
                        })
                    }
                </div>
            </div>
            
        </div>
    )
}

export default Suugestions