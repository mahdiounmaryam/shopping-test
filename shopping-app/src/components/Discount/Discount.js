import React from 'react';
import SectionTitle from '../SectionTitle/SectionTitle';
import RedDiscount from '../../assets/icons/RedDiscount.svg';
import ReactSlider from "react-slider";

const Discount = () => {
    return (
        <div className='mt-4'>
            <SectionTitle image={RedDiscount} hasShowMore={true} title='تخفیف های ویتسل'/>
            <ReactSlider  marks={20}
          min={0}
          max={100} />
        </div>
    )
}

export default Discount