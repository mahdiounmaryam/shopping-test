import Email from '../../../assets/icons/email.svg';
import Phone from '../../../assets/icons/Phone.svg';
import Address from '../../../assets/icons/Address.svg';

export const FooterData = [
    {
        id:1,
        title : 'با ویستل',
        subMenu : [
            {id : 1 , title : 'درباره ما'},
            {id : 2 , title : 'تماس با ما'},
            {id : 3 , title : 'حریم خصوصی'},
            {id : 4 , title : 'شرایط بازگشت کالا'},
            
        ]
    },
    {
        id:2,
        title : 'محصولات ویستل',
        subMenu : [
            {id : 1 , title : 'کالای دیجیتال'},
            {id : 2 , title : 'سوپر مارکت'},
            {id : 3 , title : 'گوشی موبایل'},
            {id : 4 , title : 'ابزار الات'},
            {id : 5 , title : 'لوازم تحریر'},
        ]
    },
    {
        id:3,
        title : 'ارتباط با ویستل',
        subMenu : [
            {id : 1 , title : 'vitdell@gmail.com' , icon : Email},
            {id : 2 , title : '0847747322' , icon : Phone},
            {id : 3 , title : 'خیابان ولیعصر نرسیده به سینما آفریقا' ,icon : Address}
        ]
    }
]