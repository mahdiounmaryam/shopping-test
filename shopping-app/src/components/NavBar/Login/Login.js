import React from 'react';
import './login.css';
import MobileLogin from '../../../assets/icons/mobileLogin.svg';

const Login = () => {
    return (
        <div>
            <div className='max1400:hidden'>
                <button className='button text-white rounded-3xl py-2 px-6 font-bold'>ورود/ثبت نام</button>
            </div>
            <div className='min1400:hidden'>
                <img src={MobileLogin}/>
            </div>
        </div>
    )
}

export default Login