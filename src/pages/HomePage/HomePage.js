import React from 'react';
import Favorites from '../../components/Favorites/Favorites';
import Discount from '../../components/Discount/Discount';
import Suugestions from '../../components/Suggestions/Suugestions';
import BestSelling from '../../components/Bestselling/Bestselling';
import Offer from '../../components/Offer/Offer';
import ImageSection from '../../components/ImageSection/ImageSection';
import DiscountImage from '../../assets/images/discount.png';
import DigiPlus from '../../assets/images/digiplus.png';
import Slider from '../../components/Slider/Slider';
import TimingSlider from '../../components/TimingSlider/TimingSlider';
import { SliderData } from '../../components/Slider/SliderData';
import { TimingSliderData } from '../../components/TimingSlider/TimingSliderData';
import Top from '../../containers/Top/top';
import GetApp from '../../components/getApp/GetApp';

const HomePage = () => {
    return (
        <div>
            <Top/>
            <ImageSection src1={DiscountImage} src2={DigiPlus}/>
            <Discount/>
            <ImageSection src1={DigiPlus} src2={DiscountImage}/>
            {/*<Favorites/>*/}
            <Suugestions/>
            {/*<BestSelling/>*/}
            <Offer/>
            <GetApp/>
        </div>
    )
}

export default HomePage