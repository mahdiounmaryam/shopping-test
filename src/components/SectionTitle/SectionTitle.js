import React from 'react';
import ShowMore from '../../assets/icons/showMore.svg';
import './sectionTitle.css';

const SectionTitle = ({image,title,hasShowMore}) => {
    return (
        <div className='flex justify-between items-center '>
            <div className='title flex items-center ml-6'>
                <div className='bg-[#F7F7F7] rounded-[26px] p-2 ml-6'>
                    <img src={image}/>
                </div>
                <h2 className='font-black text-base min-w-[200px]'>{title}</h2>
                <div className='bg-[#FFE9EE] h-[1px] w-full'></div>
            </div>
            
            {hasShowMore && 
                <div className='flex max1400:hidden gap-1 w-[110px]'>
                    <span>مشاهده همه</span>
                    <img src={ShowMore}/>
                </div>
            }
        </div>
    )
}

export default SectionTitle