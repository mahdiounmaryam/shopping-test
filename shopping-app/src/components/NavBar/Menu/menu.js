import React from 'react';
import { MenuData } from './menuData';
import MenuItem from './menuItem';
import More from '../../../assets/icons/more.svg';
import HeaderLogo from '../../../assets/icons/headerLogo.svg';
import Hamberger from '../../../assets/icons/hamberger.svg';

const menu = () => {
  return (
    <ul className='flex min1400:justify-between p-0 min1400:w-[450px]'>
      <li className='max1400:hidden'>
        <img src={HeaderLogo}/>
      </li>
      
        {
          MenuData.map(menuItem=>{
            return (
              <MenuItem 
                key={menuItem.id} 
                title={menuItem.title} 
                icon={menuItem.icon}
                more={More}
                hasSubmenu={menuItem.submenu.length==0 ? false : true}
              />
            )
          })
      }
      <div className='min1400:hidden'>
        <img src={Hamberger}/>
      </div>
    </ul>
  )
}

export default menu