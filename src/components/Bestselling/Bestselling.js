import React from 'react';
import SectionTitle from '../SectionTitle/SectionTitle';
import BestsellingIcon from '../../assets/icons/BestSelling.svg';

const Bestselling = () => {
    return (
        <div>
            <SectionTitle hasShowMore={true} image={BestsellingIcon} title='پرفروش ترین محصولات'/>
        </div>
    )
}

export default Bestselling