import React from 'react';
import Slider from '../../components/Slider/Slider';
import { SliderData } from '../../components/Slider/SliderData';
import { TimingSliderData } from '../../components/TimingSlider/TimingSliderData';
import './top.css';
import CarouselSlider from '../../components/CarouselSlider/CarouselSlider';
import Slider2 from '../../components/Slider2/Slider2';

const top = () => {
    return (
        <section className='flex mt-4 gap-4'>
            <div className='main-slider'>
                <Slider SliderData={SliderData}/>
            </div>
            
            <div className='max1400:hidden w-[282px] bg-[#F7F7F7] rounded-3xl'>
                <CarouselSlider data={TimingSliderData}/>
            </div>
        </section>
    )
}

export default top