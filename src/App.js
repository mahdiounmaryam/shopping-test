import {BrowserRouter as Router,Routes,Route} from 'react-router-dom';
import Layout from './components/Layout/Layout'
import {routes} from './routes';


function App() {
  return (
    <div className='px-[120px] max1400:px-[16.5px] pt-8 overflow-x-hidden'>
      
        <Router>
          <Layout>
          <Routes>
            {routes.map(route=>{
              return <Route key={route.id} path={route.path} element={route.element} />
            })}
          </Routes>
          </Layout>
        </Router>
    </div>
    
  );
}

export default App;
