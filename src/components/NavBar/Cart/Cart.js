import React from 'react';
import ShoppingCart from '../../../assets/icons/shopping-cart.svg';
import Cartmobile from '../../../assets/icons/cartMobile.svg';

const Cart = () => {
    return (
        <div>
            <div className='bg-[#F7F7F7] h-12 w-12 relative rounded-[24px] max1400:hidden'>
                <img className='absolute top-[24%] right-[26%]' src={ShoppingCart}/>
            </div>
            <div className='min1400:hidden'>
                <img src={Cartmobile}/>
            </div>
        </div>
    )
}

export default Cart