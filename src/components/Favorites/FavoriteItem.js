import React from 'react';


const FavoriteItem = ({categoryName,image}) => {
    return (
        <div className='bg-[#F2F2F2] rounded-2xl w-fit p-2 flex flex-col items-center'>
            <img src={image}/>
            <h3 className='text-black font-bold text-sm'>{categoryName}</h3>
        </div>
    )
}

export default FavoriteItem