import React from 'react';
import './navbarStyles.css';
import Menu from './Menu/menu';
import SearchBox from './searchBox/searchbox';
import Cart from './Cart/Cart';
import Login from './Login/Login';

const NavBar = () => {
    return (
        <nav className='navbar rounded-[40px] flex justify-between items-center gap-2 p-6 h-20 '>
            <Menu/>

            <div className='flex items-center justify-between gap-2 w-[444px]'>
                <SearchBox/>
                <Cart/>
                <Login/>
            </div>
        </nav>
    )
}

export default NavBar