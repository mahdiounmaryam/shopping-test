import Image1 from '../../assets/images/favorites/image1.png';
import Image2 from '../../assets/images/favorites/image2.png';
import Image3 from '../../assets/images/favorites/image3.png';
import Image4 from '../../assets/images/favorites/image4.png';
import Image5 from '../../assets/images/favorites/image5.png';
import Image6 from '../../assets/images/favorites/image6.png';
import Image7 from '../../assets/images/favorites/image7.png';
import Image8 from '../../assets/images/favorites/image8.png';
import Image9 from '../../assets/images/favorites/image9.png';

export const FavoriteData = [
    {
        id : '1',
        image : Image1 ,
        categoryName : 'مد و پوشاک'
    },
    {
        id : '2',
        image : Image2 ,
        categoryName : 'موبایل'
    },
    {
        id : '3',
        image : Image3 ,
        categoryName : 'کالای دیجیتال'
    },
    {
        id : '4',
        image : Image4 ,
        categoryName : 'لوازم تحریر'
    },
    {
        id : '5',
        image : Image5 ,
        categoryName : 'تجهیزات صنعتی'
    },
    {
        id : '6',
        image : Image6 ,
        categoryName : 'سوپر مارکت'
    },
    {
        id : '7',
        image : Image7 ,
        categoryName : 'اسباب بازی'
    },
    {
        id : '8',
        image : Image8 ,
        categoryName : 'زیبایی و سلامت'
    },
    {
        id : '9',
        image : Image9,
        categoryName : 'خانه و آشپزخانه'
    },
]