import React from 'react';
import Navbar from '../../components/NavBar/NavBar';

const Header = () => {
    return (
        <header>
            <Navbar/>
        </header>
    )
}

export default Header