import React, { useState,useEffect } from 'react';
import RightArrow from '../RightArrow/RightArrow';
import LeftArrow from '../LeftArrow/LeftArrow';
import './Slider.css';

const Slider = ({SliderData,extraData}) => {
  const [index, setIndex] = useState(0);

  const nextSlide = () => {
    setIndex((oldIndex) => {
      let index = oldIndex + 1
      if (index > SliderData?.length - 1) {
        index = 0
      }
      return index
    })
  }

  const prevSlide = () => {
    setIndex((oldIndex) => {
      let index = oldIndex - 1
      if (index < 0) {
        index = SliderData?.length - 1
      }
      return index
    })
  }

  useEffect(() => {
    let slider = setInterval(() => {
      setIndex((oldIndex) => {
        let index = oldIndex + 1
        if (SliderData && index > SliderData?.length - 1) {
          index = 0
        }
        return index
      })
    }, 3000)
    return () => {
      clearInterval(slider)
    }
  }, [index])

  return (
    <section className='section'>
      <div className='section-center'>
        {SliderData && SliderData.map((SingleData, SingleDataIndex) => {
          const { id, image } = SingleData;

          let position = 'nextSlide'
          if (SingleDataIndex === index) {
            position = 'activeSlide'
          }
          if (
            SingleDataIndex === index - 1 ||
            (index === 0 && SingleDataIndex === SliderData?.length - 1)
          ) {
            position = 'lastSlide'
          }

          return (
            <div>
              <article key={id} className={position}>
                <img src={image} className='slideImage' />
                {extraData && <div className='h-[117px] absolute right-0 left-0 bottom-0 bg-red-400'>extra</div>}
              </article>
            </div>
          )
        })
      }
        <div className='absolute cursor-pointer top-[37%] -left-[1px]' onClick={prevSlide}>
          <LeftArrow/>
        </div>

        <div className='absolute cursor-pointer top-[37%] -right-[1px]' onClick={nextSlide}>
          <RightArrow/>
        </div>
    
      </div>
    </section>
  )
}

export default Slider