import React from 'react';
import Back1 from '../../assets/images/back1.png';
import Back2 from '../../assets/images/back2.png';
import Image1 from '../../assets/images/image1.png';
import Image2 from '../../assets/images/image2.png';
import Bazar from '../../assets/icons/Bazar.svg';
import Miget from '../../assets/icons/Miget.svg';
import Download from '../../assets/icons/download.svg';
import SingleConnection from './singleConnection';

const GetApp = () => {
    const data = [
        {id:1 , src : Bazar , text : 'دانلود از بازار'},
        {id:1 , src : Miget , text : 'دانلود از مایگت'},
        {id:1 , src : Download , text : 'دانلود مستقیم'}
    ]
    return (
        <div className='min1400:h-[350px] max1400:mt-5 max1400:w-full items-end flex mb-20 select-none'>
            <div className='bg-[#CE273B] min1400:w-[888px] max1400:w-full min1400:h-[155px] flex justify-around rounded-3xl p-4 mx-auto min1400 :my-48'>
                <div className='max1400:hidden'>
                    <p className='text-white py-4 text-lg font-black'>ویتسل را همیشه همراه داشته باشید!</p>
                    <div className='flex gap-4'>
                        {data.map(singleData=>{
                            return <SingleConnection key={singleData.id} text={singleData.text} src={singleData.src}/>
                        })}
                    </div>
                </div>
                <div className='min1400:hidden flex flex-col gap-2'>
                    <p className='text-white py-4 text-lg font-black'>ویتسل را همیشه همراه داشته باشید!</p>
                    <div className='flex gap-2'>
                        <SingleConnection key={data[0].id} text={data[0].text} src={data[0].src}/>
                        <SingleConnection key={data[1].id} text={data[1].text} src={data[1].src}/>
                    </div>
                    <SingleConnection key={data[2].id} text={data[2].text} src={data[2].src}/>
                </div>
                <div className='max1400:hidden relative bottom-[142px]'>
                    <div className='relative top-[5px]'>
                        <img src={Back1}/>
                        <img className='absolute top-[3px] right-1 rounded-xl' src={Image1}/>
                    </div> 
                    <div className='relative bottom-[239px] left-[57px]'>
                        <img src={Back2}/>
                        <img className='absolute top-[5px] right-[2px] rounded-xl' src={Image2}/>
                    </div> 
                </div>
            </div>
        </div>
    )
}

export default GetApp