import React from 'react'

const ItemDetails = ({title,percent,price,totalPrice}) => {
    return (
        <div>
            <h3 className='font-bold text-[12px] ml-[42.5px]'>{title}</h3>
            <div>
                <div className='absolute left-3 bottom-[42px] flex items-center'>
                    <span className='font-black text-lg'>{price}</span>    
                    <span className='text-xs font-normal mr-[1px]'>تومان</span>
                </div>
                <div className='absolute left-[10px] bottom-4 line-through text-[#BDBDBD]'>
                    <span className='text-xs font-normal'>{totalPrice}</span>
                </div>
                <div className='percent bottom-3 right-[104px] text-white rounded-[80px] absolute py-1 px-3'>
                    <span className='text-[12px] font-bold'>{percent}%</span>
                </div>
            </div>
        </div>
    )
}

export default ItemDetails