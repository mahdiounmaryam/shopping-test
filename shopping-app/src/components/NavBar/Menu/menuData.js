import Category from '../../../assets/icons/category.svg';
import Fire from '../../../assets/icons/fire.svg';
import Discount from '../../../assets/icons/discount.svg';

export const MenuData=[
    {
        id : 1,
        title : 'دسته بندی‌ ها' ,
        icon : Category,
        submenu : [
            {id:1}
        ]
    },
    {
        id : 2,
        title : 'پرفروش ترین‌ ها' ,
        icon : Fire,
        submenu : []
    },
    {
        id : 3,
        title : 'تخفیف‌ دار‌ ها',
        icon : Discount,
        submenu : []
    },
]