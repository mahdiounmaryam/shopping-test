import React from 'react';
import Header from '../../containers/Header/Header';
import { Fragment } from 'react';
import Footer from '../../containers/Footer/Footer';

const Layout = (props) => {
    return (
        <Fragment>
            <Header/>
                {props.children}
            <Footer/>
        </Fragment>
    )
}

export default Layout