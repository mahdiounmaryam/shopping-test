import React from 'react'

const ImageSection = ({src1,src2}) => {
    return (
        <section className='flex gap-4 mt-4'>
            <div className='w-[50%]'>
                <img className='rounded-3xl' src={src1}/>
            </div>
            <div className='w-[50%]'>
                <img className='rounded-3xl' src={src2}/>
            </div>
        </section>
    )
}

export default ImageSection