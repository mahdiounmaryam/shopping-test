/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./src/**/*.{js,jsx,ts,tsx}",
    ],
    theme: {
        extend: {
            screens: {
                'max1400': {'max': '1400px'},
                'min1400': {'min': '1400px'},
            }
        },
    },

    plugins: [],
}