import React from 'react';
import FooterLogo from '../../assets/images/Logo.png';
import FooterMenu from './FooterMenu/FooterMenu';

const Footer = () => {
    return (
        <footer className='flex max1400:flex-wrap max1400:mt-6 gap-1 order-8 my-4'>
            <div className=' w-[250px] max1400:hidden'>
                <img className='mb-1' src={FooterLogo}/>
                <div className=' w-full '>
                    <h2 className='font-black text-base mb-1'>ویتسل</h2>
                    <p className='text-[12px]'>
                        صرافی هفت ارز پلتفرم ایمن و سریع برای انجام معاملات ارز دیجیتال در بستری مطمئن
                    </p>
                </div>
            </div>
            <FooterMenu/>
        </footer>
    )
}

export default Footer