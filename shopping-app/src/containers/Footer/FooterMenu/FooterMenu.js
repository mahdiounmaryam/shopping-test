import React from 'react';
import { FooterData } from './FooterData';
import './footer.css';

const FooterMenu = () => {
    return (
        <div className='footermenu-wrapper flex max1400:flex-wrap '>
            {FooterData.map(data=>{
                return(
                    <ul className='flex flex-col gap-4 w-1/2' ley ={data.id}>
                        <li className='text-base font-black max1400:mt-3'>{data.title}</li>
                        {
                            data.subMenu && data.subMenu.map(sub=>{
                                return(
                                    <ul key={sub.id}>
                                        <li className='flex gap-1 text-[12px]'>
                                            {sub.icon && <img src={sub.icon}/>}
                                            <span >{sub.title}</span>
                                        </li>
                                    </ul>
                                )
                            })
                        }
                    </ul>
                )
            })}
        </div>
    )
}

export default FooterMenu