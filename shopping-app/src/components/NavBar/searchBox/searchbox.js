import React from 'react';
import Search from '../../../assets/icons/search.svg';

const searchbox = () => {
    return (
        <div className='relative'>
            <input type='text' className='bg-[#F7F7F7] outline-none p-3 rounded-[32px] placeholder:text-sm' placeholder='جستجوی محصولات...'></input>
            <img className='absolute top-[15.04px] left-[13.04px]' src={Search}/>
        </div>
    )
}

export default searchbox