import React from 'react';
import DiscountFireSvg from '../DiscountFireSvg/DiscountFireSvg';
import ItemDetails from '../ItemDetails/ItemDetails';

const singleSuggestion = ({image,title,totalPrice,price,percent}) => {
    return (
        <div className='bg-[#F7F7F7] max1400:min-w-[274px] relative flex p-2 rounded-3xl'>
            <img src={image}/>
            <div>
                <div className='absolute top-0 left-[7px]'>
                    <DiscountFireSvg/>
                </div>
                
                <ItemDetails title={title} price={price} totalPrice={totalPrice} percent={percent}/>
            </div>
        </div>
    )
}

export default singleSuggestion