import React from 'react';
import bg from '../../assets/icons/leftArrowBg.svg';
import arrow from '../../assets/icons/leftArrow.svg';

const LeftArrow = () => {
    return (
        <div className='relative'>
            <img src={bg}/>
            <img src={arrow} className='absolute top-[43%] left-[33%]'/>
        </div>
    )
}

export default LeftArrow