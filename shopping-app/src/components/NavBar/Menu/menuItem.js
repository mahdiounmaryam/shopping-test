import React from 'react';
import { Link } from 'react-router-dom';

const menuItem = ({icon,title,more,hasSubmenu}) => {
    return (
        <li className='flex items-center gap-1 max1400:hidden'>
            <img className='w-5 h-5' src={icon}/>
            <Link to='/' className='text-sm font-normal'>{title}</Link>
            {hasSubmenu && <img className='w-3' src={more}/>}
        </li>
    )
}

export default menuItem