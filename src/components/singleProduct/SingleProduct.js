import React from 'react'

const SingleProduct = ({image,title,price}) => {
    return (
        <div className='bg-[#F7F7F7] rounded-3xl relative h-full  w-[282px]'>
            <img className='m-auto' src={image}/>
            <div className='absolute h-[112px] w-full bottom-0'>
                <h2 className='font-black text-base mr-[16px]'>{title}</h2>
                <div className='absolute left-4 bottom-4'>
                    <span className='text-xl font-black'>{price}</span>
                    <span className='text-xs font-normal mr-[2px]'>تومان</span>
                </div>
            </div>
        </div>
    )
}

export default SingleProduct