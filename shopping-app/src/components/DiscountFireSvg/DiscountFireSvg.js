import React from 'react';
import FireBg from '../../assets/icons/fireBg.svg';
import RedFire from '../../assets/icons/redFire.svg';

const DiscountFireSvg = () => {
    return (
        <div className='relative w-fit rounded-tl-[15px]'>
            <img className='rounded-tl-[15px]' src={FireBg}/>
            <img className='absolute top-1 right-1' src={RedFire}/>
        </div>
    )
}

export default DiscountFireSvg