import React from 'react';
import RedCategory from '../../assets/icons/RedCategory.svg';
import { FavoriteData } from './FavoriteData';
import FavoriteItem from './FavoriteItem';
import Cart from '../../assets/images/cart.png';
import SectionTitle from '../SectionTitle/SectionTitle';

const Favorites = () => {
    return (
        <div className='flex mt-4'>
            <div className='flex flex-col gap-6'>
                <SectionTitle hasShowMore={false} image={RedCategory} title='دسته بندی های محبوب'/>
                <div className='flex flex-wrap gap-6'>
                    {
                        FavoriteData.map(fav=>{
                            return <FavoriteItem key={fav.id} categoryName={fav.categoryName} image={fav.image}/>
                        })
                    }
                </div>
            </div>
            <img src={Cart}/>
        </div>
    )
}

export default Favorites