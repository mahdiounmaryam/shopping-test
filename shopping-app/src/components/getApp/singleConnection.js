import React from 'react';

const singleConnection = ({src,text}) => {
    return (
        <div className='bg-[#ffffff1c] flex rounded-3xl justify-center py-3 px-4'>
            <img src={src}/>
            <span className='text-white text-sm font-bold'>{text}</span>
        </div>
    )
}

export default singleConnection